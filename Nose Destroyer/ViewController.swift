//
//  ViewController.swift
//  Nose Destroyer
//
//  Created by Paul Pearson on 2/20/16.
//  Copyright © 2016 RPM Consulting. All rights reserved.
//

import UIKit
import AVFoundation

var playCounter:Int = 1

class ViewController: UIViewController {
    
    var player: AVAudioPlayer = AVAudioPlayer()
    @IBOutlet var backgroundImage: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        /*
        let audioPath = NSBundle.mainBundle().pathForResource("fart_1", ofType: "mp3")!
        do {
            try player = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: audioPath))
            player.play()
        } catch {
            print("couldn't find the audio file")
        }
        */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
        
        if event?.subtype == UIEventSubtype.MotionShake {
            var audioPath:String = ""
            
            print("device was shaken")
            // get a random number from 1 to 4...
            let random = Int(arc4random_uniform(4)) + 1
            print("Random number is: \(random), counter: \(playCounter)")
            backgroundImage.image = UIImage(named: "flower\(random).jpg")
            if playCounter % 10 == 0 {
                audioPath = NSBundle.mainBundle().pathForResource("firework", ofType: "mp3")!
            } else {
                audioPath = NSBundle.mainBundle().pathForResource("fart_\(random)", ofType: "mp3")!
            }
            do {
                try player = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: audioPath))
                player.play()
                playCounter += 1
            } catch {
                print("couldn't find the audio file")
            }
        }
    }


}

